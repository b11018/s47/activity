console.log(`s47 - Introduction to DOM ACTIVITY`);

console.log(document);
// result: the document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);

/*
	alternative ways:
		>> document.getElementById("txt-first-name")
		>> document.getElementByClassName("text-class")
		>> document.getElementByTagName("h1")
*/

// targets the full name
const spanFullName = document.querySelector("#span-full-name")

// event listeners
// (event) can have a shorthand of (e)
// Syntax: addEventListener('stringOfAnEvent', function)
const listenFirstName = (event) => {
	spanFullName.innerHTML = txtFirstName.value
};
txtFirstName.addEventListener('keyup', listenFirstName);

// multiple listeners
const logEvents = (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
};
txtFirstName.addEventListener('keyup', logEvents);

// ACTIVITY

const txtLastName = document.querySelector("#txt-last-name");

const updateSpan = (event) => {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
};
txtLastName.addEventListener('keyup', updateSpan);